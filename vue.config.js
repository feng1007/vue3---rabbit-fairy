const path = require('path')
module.exports = {
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'less', // 自动注入less文件到页面style中
      // 指定注入什么文件
      patterns: [
        // 配置哪些文件需要自动导入
        path.join(__dirname, '/src/styles/variables.less')
      ]
    }

  }
}
