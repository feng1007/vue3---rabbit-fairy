import { createRouter, createWebHashHistory } from 'vue-router'
// 公共布局(父路由一级)
const Layout = () => import('@/views/Layout')
// 子路由=>二级
// 首页
const Home = () => import('@/views/Home')

// 路由配置规则
const routes = [
  {
    path: '/',
    component: Layout,
    children: [
      { path: '/', component: Home }
    ]
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
