import axios from 'axios'
import store from '@/store'
import router from '@/router'
// baseURL 超时时间配置
const instance = axios.create({
  baseURL: 'http://pcapi-xiaotuxian-front-devtest.itheima.net'
})

// 全局注入token
// 请求拦截器=>发请求前
instance.interceptors.request.use(config => {
  // 在请求头统一添加token
  const { token } = store.state.user.profile
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }
  return config
}, e => Promise.reject(e))

// 处理返回数据 token失效跳回到登录页
// 响应拦截器=>成功请求
instance.interceptors.response.use(
  (response) => {
    // 请求200进入到这里
    // 把data数据返回给页面
    return response.data
  },
  (error) => {
    // 200以外进入
    // 处理401
    if (error?.response.status === 401) {
      /*
      1.获取当前出现401的页面路径(目的:成功登录之后,回到上次访问的页面)
      2.调回登录页带上401页面的地址
      */
      const redirectUrl = router.currentRoute.value.fullPath
      router.replace(`/login?redirectUrl=${redirectUrl}`)
    }
    return Promise.reject(error)
  }
)

/*
二次封装(不是必须的)
基于:instance的axios实例
 @param {*} url:string 请求路径
 * @param {*} method:string 请求方法（get/post等）
 * @param {*} datas:object 请求的时候需要的参数
*/
const request = (url, method, datas) => {
  // 返回promise对象
  return instance({
    url,
    method,
    // 对象动态属性名
    [method.toLowerCase() === 'get' ? 'params' : 'data']: datas
  })
}
export default request
