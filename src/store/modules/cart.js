// 购物车状态
export default {
  namespaced: true,
  state: () => ({
    // 购物车列表数据
    list: []
  })
}
