/*
存储菜单导航数据
*/
import { findHeadCategory } from '@/api/home'
export default {
  namespaced: true,
  state: {
    cateList: [] // 菜单分类数据
  },
  // 加载数据成功后需要修改list所以需要mutations函数
  mutations: {
    /*
    @param {*} state 获取变量对象
    @param {*} cateList 传入的菜单数据
    */
    updateCateList (state, cateList) {
      state.cateList = cateList
    }
  },
  // 需要向后台加载数据，所以需要actions函数获取数据
  actions: {
    // 获取导航菜单数据
    async getCateList ({ commit }) {
      // 1.调用接口方法
      // 2.成功获取到数据之后触发mutation函数
      const { result } = await findHeadCategory()
      console.log(111, result)
      commit('updateCateList', result)
    }
  }
}
