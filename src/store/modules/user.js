// 用户状态
export default {
  namespaced: true, // 开启模块化,避免命名的冲突
  // 定义变量
  state: () => ({
    // 登录人信息
    profile: {
      id: '',
      avatar: '',
      nickname: '',
      account: '',
      mobile: '',
      token: ''
    }
  }),
  mutations: {
    // 测试:修改nickname
    // updateName (state, payload) {
    //   state.profile.nickname = payload
    // }
  }
}
