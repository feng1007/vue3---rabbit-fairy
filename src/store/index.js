import { createStore, createLogger } from 'vuex'
// 导入持久化插件
import createPersistedstate from 'vuex-persistedstate'

// 导入模块
import user from './modules/user'
import cart from './modules/cart'
import category from './modules/category'

// 创建store实例
export default createStore({
  // 1.定义变量
  state: {
  },
  // 2.提供修改变量的方法(同步)
  mutations: {
  },
  // 3.获取后台数据(异步)
  actions: {
  },
  // vuex模块化
  modules: {
    user,
    cart,
    category
  },
  // 插件(穿装备)
  // 1.默认存所有数据 2.通过key指定存储名,通过paths指定存什么数据
  plugins: [createPersistedstate(
    {
      key: 'rabbit-137', // 指定存储的属性名
      paths: ['user', 'cart'] // 指定存什么数据
    }
  ),
  // 操作vuex数据,控制台会有日志
  createLogger()
  ]
})
